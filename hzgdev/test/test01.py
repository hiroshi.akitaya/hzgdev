"""
Test code #1 for simimg.py
    2022-01-24  H. Akitaya
"""


import astropy.units as u
import numpy as np

from hzgdev.phot import simimg


if __name__ == '__main__':
    # Instrumental parameters.
    ron = 18  # Readout noise (e-/pix).
    dark_rate = 0.3 / u.s  # Dark count rate (count/u.s).
    bias_level = 0.0  # Bias offset level. (count).
    d_tel = 0.3 * u.m  # Diameter of telescope (u.m).
    t_exp = 120.0 * u.s  # Exposure time (u.s).
    eff_inst = 0.9 * 0.7  # Efficiency of instrument.
    pix_fov = (2.0 * u.arcsec) ** 2  # Pixel FoV (u.arcsec**2).
    ecliptic_latitude = 45.0  # Ecliptic latitude (deg).
    fwhm = 1.5  # FWHM of stellar image (pix).
    band = 'o'  # Band name ('o', 'j', 'h', or 'k').

    # Image parameters.
    shape = (64, 64)

    # Read catalog text data to qtable
    fn_catalog = 'test_catalog_o.xy'
    tbl = simimg.make_qtable(simimg.read_uvista_xyfile(fn_catalog), band, d_tel, t_exp, eff_inst, fwhm)
    # Apply subpixel dithering.
    dither_x = 2.4  # Dithering x. (pixel)
    dither_y = 2.2  # Dithering y. (pixel)
    tbl_dither = simimg.qtable_dither(tbl, dither_x, dither_y)
    print(tbl_dither)

    # Make artificial stellar image without noise.
    img_stellar = simimg.make_stellar_image_ndarray(tbl_dither, shape, fn='test_img.npy', show_elapsed_time=True)
    # Make bias image with readout noise.
    img_bias = simimg.make_bias_image_ndarray(shape, ron, bias_level)
    # Get zodiacal light count level.
    zd_level = simimg.get_zd_level_at_band('o', ecliptic_latitude, t_exp, d_tel, eff_inst, pix_fov)
    # Get dark count level.
    dark_level = simimg.get_dark_level(dark_rate, t_exp)
    # Co-add stellar image and background levels.
    img_w_bg = simimg.add_zd_and_dark(img_stellar, zd_level, dark_level)
    # Make final image array with noise.
    img_w_noise = simimg.add_poisson_noise(img_w_bg) + img_bias

    # Write to fits file.
    fn_fits = 'test_artificial_o.fits'
    simimg.write_to_fitsfile(img_w_noise, fn_fits, overwrite=True)

    print(zd_level)
    print(np.median(img_w_noise), np.std(img_w_bg), np.std(img_w_noise))
    # print(img_bias)
