"""
HiZ-GUNDAM Photometry simulation tools.

　　2022-05-10 H. Akitaya
"""

import os
import sys
from typing import Tuple, List

from astropy.io import fits
import astropy.units as u
import numpy as np
from astropy import constants as const
from astropy.table import QTable
import photutils.datasets as photu_dt

from scipy import integrate

from zodiacal.zodiacallight import ZodiacalLight

# HZG default band set at 2022 May.
BAND_WLS = {'o': [0.5 * u.um, 0.9 * u.um], 'j': [0.9 * u.um, 1.5 * u.um],
            'h': [1.5 * u.um, 2.0 * u.um], 'k': [2.0 * u.um, 2.5 * u.um]
            }

class HZGParams(object):

    def __init__(self):
        """
        Initialize.
        """
        self.bands = ['o', 'j', 'h', 'k']  # Band name definition .(list)
        self.band_wls = BAND_WLS  # Band wavelength ranges. (dict)
        self.pix_fov = (2.0 * u.arcsec) ** 2  #
        self.eff_tel = 0.9 * 0.7  # Efficiency of telescope.
        self.d_tel = 0.3 * u.m  # Diameter of telescope.
        self.t_exp = 120.0 * u.s  # Exposure time.
        self.elat = 45.0  # Ecliptic latitude. (deg)
        self.fwhm = 1.5  # FWHM of stellar image. (pix)
        self.darkrate = 0.3 / u.s  # Dark current rate. (e-/s/pix)
        self.ron = 18.0  # Readout noise. (e-/pix)
        self.pixsize = 18.0 * u.um  # Pixel size.
        self.n_pix = 9  # Number of pixels per aperture.  # 2022-05-10
        self.n_frames = 5  # Number of frames to be taken.  # 2022-05-10

    def show(self):
        """
        Print Parameters to stdout.
        :return: int
        """
        print('Band names: {}'.format(self.bands))
        print('Band Wavelength Definitions: {}'.format(self.band_wls))
        print('FOV per pixel: {}'.format(self.pix_fov))
        print('Efficiency of instrument: {}'.format(self.eff_tel))
        print('Diameter of telescope: {}'.format(self.d_tel))
        print('Exposure time per one frame {}'.format(self.t_exp))
        print('Ecliptic latitude (deg): '.format(self.elat))
        print('FWHM of stellar image: '.format(self.fwhm))
        print('Dark count rate: '.format(self.darkrate))
        print('Readout noise (e-/pix)',format(self.ron))
        return 0


def __fphoton_integ(l1: float, l2: float):
    """
    Integrate wavelength range in a band.
    :param l1: Wavelength 1 (begin) [um]
    :param l2: wavelength 2 (end) [um]
    :return:
    """
    result = integrate.quad(lambda x: __fphoton(x), l1, l2)
    return result[0]


def __fphoton(wl: float) -> float:
    """
    :param wl: wavelength [m]
    :return: photon flux [1/s/m/cm**2]
    """
    return 5479538.347026109 / wl


def get_zeromag(band_wls: dict) -> dict:
    """
    Calculate zeromag photon flux for 'bald_wls' dictionary.
    :param band_wls: Dictionary with items; 'bandname': [wl1 (u.um), wl2 (u.um)].
    :return: zeromag photon flux dictionary; 'bandname': zeromag (photons/u.s/u.cm**2).
    """
    c_zeromag = {}
    for band in band_wls.keys():
        wl1 = band_wls[band][0].to(u.um).value
        wl2 = band_wls[band][1].to(u.um).value
        c_zeromag_tmp = __fphoton_integ(wl1, wl2)
        c_zeromag[band] = c_zeromag_tmp / u.s / u.cm ** 2
    return c_zeromag


# Define zero-mag constants for each bands.
C_ZEROMAG = get_zeromag(BAND_WLS)


def fnu_to_flambda(fnu: u.Quantity, wl: u.Quantity) -> u.Quantity:
    """
    Convert Fnu to Flambda.
    :param fnu: Flux density.
    :param wl:  Wavelength.
    :return: Flux density at a wavelength (erg/s/cm**2/um).
    """
    flambda = (fnu*const.c/wl**2).to(u.erg/u.s/u.um/u.cm**2)
    return flambda


def flambda_to_fphoton(flambda: u.Quantity, wl: u.Quantity) -> u.Quantity:
    """
    Convert Flambda to Photon flux density.
    :param flambda: Flux density at wavelength lambda.
    :param wl: Wavelength.
    :return: Photon Flux (photons/cm**2/s/um).
    """
    fphoton = (flambda/(const.h*const.c/wl)).to(1/u.cm**2/u.s/u.um)
    return fphoton


def photons_total(mag_ab: u.Quantity, wl: u.Quantity, dwl: u.Quantity, d_tel: u.Quantity,
                  t_exp: u.Quantity, eff_inst: float) -> u.Quantity:
    """
    Calculate total number of Photons.
    :param mag_ab: AB magnitude of the source.
    :param wl: Wavelength.
    :param dwl: Wavelength width.
    :param d_tel: Diameter of Telescope.
    :param t_exp: Exposure Time.
    :param eff_inst: Efficiency of the instrument.
    :return: Total number of photons.
    """
    fnu = mag_ab.to(u.erg / u.s / u.cm ** 2 / u.Hz)
    flambda = fnu_to_flambda(fnu, wl)
    fphoton = flambda_to_fphoton(flambda, wl)
    photons = (fphoton * dwl * (np.pi * (d_tel / 2.0) ** 2) * t_exp * eff_inst).decompose()
    return photons


def photons_total_at_band(mag_ab, band, dtel_cm, t_exp_s, eff_tel, c_zeromag=None):
    if c_zeromag is None:
        c_zeromag = C_ZEROMAG
    fphoton = c_zeromag[band] * 10 ** (-2.0 / 5.0 * mag_ab.value)
    photons = (fphoton * (np.pi * (dtel_cm / 2.0) ** 2) * t_exp_s * eff_tel).decompose()
    return photons


def read_uvista_xyfile(fn: str) -> Tuple[List[float], List[float], List[float]]:
    """
    Read UltraVISTA catalog data into QTable.
    :param fn: File name (UltraVISTA field).
    :return: QTable of objects.
    """
    x_list = []
    y_list = []
    mag_list = []
    with open(fn, 'r') as f:
        print('File open: {}'.format(fn))
        for line in f.readlines():
            try:
                values = line.strip().split()
                x_list.append(float(values[0]))
                y_list.append(float(values[1]))
                mag_list.append(float(values[2]))
            except ValueError:
                # sys.stderr.write('Error in reading line.\n')
                pass
    return x_list, y_list, mag_list


def make_qtable(mag_table: Tuple[List[float], List[float], List[float]], band: str, d_tel: u.Quantity,
                t_exp: u.Quantity, eff_inst: float, fwhm: float) -> QTable:
    """
    Make QTable of artificial image from (x, y, flux) list.
    :param mag_table: Magnitude table list. (x[], y[], mag[]).
    :param band: Band name ('o', 'j', 'h', 'k').
    :param d_tel: Telescope diameter (u.m) [m].
    :param t_exp: Exposure time (u.s) [sec].
    :param eff_inst: Instrument efficiency.
    :param fwhm: FWHM of stellar image [pixels].
    :return: QTable
    """
    x_list = mag_table[0]
    y_list = mag_table[1]
    mag_list = mag_table[2]

    # Convert FWHM to standard deviation for Gaussian function.
    stddev = fwhm / (2.0 * np.sqrt(2.0 * np.log(2.0)))

    tbl: QTable = QTable()  # Write to table.
    obj_c = []
    for mag in mag_list:
        obj_c.append(photons_total_at_band(mag * u.ABmag, band, d_tel, t_exp, eff_inst))
    obj_c_nd = np.array(obj_c)
    tbl['flux'] = obj_c_nd  # Stellar flux [electrons]
    tbl['x_mean'] = np.array(x_list)
    tbl['y_mean'] = np.array(y_list)
    tbl['x_stddev'] = np.ones(len(x_list)) * stddev
    tbl['y_stddev'] = np.ones(len(x_list)) * stddev
    tbl['mag'] = np.array(mag_list)
    tbl.meta['dither'] = np.array([0, 0])
    return tbl  # Return QTable.


def qtable_dither(tbl: QTable, dither_x: float, dither_y: float) -> QTable:
    """
    Dithering stellar image data.
    (Attention: tbl is pointer of the original data. Original data is modified by this function.)
    :param tbl:
    :param dither_x: x-direction dithering parameter. (pix)
    :param dither_y: y-direction dithering parameter. (pix)
    :return:
    """
    tbl['x_mean'] = tbl['x_mean'] + dither_x
    tbl['y_mean'] = tbl['y_mean'] + dither_y
    tbl.meta['dither'] = tbl.meta['dither'] + np.array([dither_x, dither_y])
    return tbl


def make_stellar_image_ndarray(tbl: QTable, shape: Tuple[float, float],
                               fn: str = None, overwrite=False, show_elapsed_time=False):
    """
    Make stellar image data as 2-d ndarray.
    :param shape: Image shape. (xsize, ysize); (e.g.) (1024, 1024)
    :param tbl: Stellar image qtable.
    :param fn: File name of ndarray. (None=Don't save ndarray file.)
    :param overwrite: Overwrite mode on/off. (Default: False)
    :param show_elapsed_time: Show execution time. (Default: False)
    :return:
    """

    import time
    t0 = time.time()  # Record time of start. (sec)
    img = photu_dt.make_gaussian_sources_image(shape, tbl)
    dt = time.time() - t0  # Elapsed time. (sec)

    # Save image array (if required).
    if fn is not None:
        if (os.path.isfile(fn) or os.path.isfile(fn + '.npy')) and overwrite is True:
            print('File {} exists. Overwrite.'.format(fn))
            try:
                if fn.endswith('.npy'):
                    os.remove(fn)
                else:
                    os.remove(fn + '.npy')
            except (FileNotFoundError, PermissionError):
                sys.stderr.write('Failed to remove {}.'.format(fn))
        try:
            np.save(fn, img)
            print('Ndarray is saved as {}.'.format(fn))
        except NameError:
            sys.stderr.write('File write error: {}'.format(fn))

    # Show elapsed time (if required).
    if show_elapsed_time:
        print('Elapsed time: {} s'.format(dt))

    return img  # Return stellar image ndarray.


def make_bias_image_ndarray(shape: Tuple, ron: float, bias_level: float = 0.0) -> np.ndarray:
    """
    Make bias image ndarray.
    :param shape: Size of image. (size_x, size_y)
    :param ron: Readout noise. (e-/pix)
    :param bias_level: Bias offset level. (Default = 0.0)
    :return: Result bias image ndarray.
    """
    img_bias = photu_dt.make_noise_image(shape, 'gaussian', bias_level, ron)
    return img_bias


def get_zd_level_at_band(band: str, ecliplat: float, t_exp: u.Quantity, d_tel: u.Quantity, inst_eff: float,
                         pix_fov: u.Quantity):
    """
    Get zodiacal light photon flux at band 'band'.
    :param band: Band name. ('o', 'j', 'h', or 'k')
    :param ecliplat: Ecliptic latitude (deg.)
    :param t_exp: Exposure time (u.Quantity: u.s or time)
    :param d_tel: Diameter of telescope. (u.Quantity: u.m or length)
    :param inst_eff: Instrumental efficiency.
    :param pix_fov: Filed of view per pixel. (u.Quantity: u.arcsec**2 or angular area)
    :return: Zodiacal light photon flux. (1/unit area/unit time/unit angular area.)
    """
    zd = ZodiacalLight()  # Require zodiacallight module.
    zd_level = (zd.get_zl_photonflux_band(band, ecliplat) * t_exp *
                np.pi*(d_tel/2)**2 * inst_eff * pix_fov).decompose().value
    return zd_level


def get_dark_level(darkrate: u.Quantity, t_exp: u.Quantity) -> float:
    """
    Get dark count level.
    :param darkrate: Dark count rate. (u.Quantity; 1/u.s)
    :param t_exp: Exposure time. (u.Quantity; u.s)
    :return: Dark count level. (no-dimension)
    """
    dark_level = darkrate * t_exp
    return dark_level


def add_zd_and_dark(img: np.ndarray, zd_level: float, dark_level: float) -> np.ndarray:
    """
    Add zodiacal light and dark level to stellar artificial image ndarray.
    :rtype: object
    :param img: Original stellar artificial image ndarray.
    :param zd_level: Zodiacal light level. (count)
    :param dark_level: Dark level. (count)
    :return: Result image ndarray.
    """
    img_w_zd_dark = img + (zd_level + dark_level)
    return img_w_zd_dark


def calc_zd_integ(wl1: float, wl2: float, ecliplat: float) -> u.Quantity:
    """
    Calculate integrated zodiacal light photon flux between wl1 and wl2.
    :param wl1: Wavelength at integration start. (um)
    :param wl2: Wavelength at integration end. (um)
    :param ecliplat: Ecliptic latitude (deg).
    :return: Photon flux of zodiacal light. (u.Quantity; 1/u.s/u.m**2/u.sr)
    """
    zd = ZodiacalLight()
    return zd.get_zl_photonflux_wlinteg(wl1, wl2, ecliplat, unit=True)


def add_poisson_noise(img: np.ndarray, cutoff_level:float = 2.0**17) -> np.ndarray:
    """
    Add Poission noise to image ndarray.
    :param img: Image to be Poisson noise applied.
    :param cutoff_level: Maximum cut-off level of the input image.
    :return: Result image array.
    """
    img1_clp = np.clip(img, 0.0, cutoff_level)  # Truncate count larger than 'cutoff_level'.
    img_clp_poisson = photu_dt.apply_poisson_noise(img1_clp)
    return img_clp_poisson


def write_to_fitsfile(img: np.ndarray, fn_out: str, overwrite=False) -> int:
    """
    Write artificial image ndarray to fits file.
    :param img: Image ndarray to be written as fits file.
    :param fn_out: File name of fits file.
    :param overwrite: Overwrite mode. (Default = False)
    :return:
    """
    hdu_b = fits.PrimaryHDU(np.float32(img))
    hdul_b = fits.HDUList([hdu_b])
    if os.path.isfile(fn_out) and overwrite is True:
        print('File {} exists. Overwrite.'.format(fn_out))
        try:
            os.remove(fn_out)
        except PermissionError:
            raise PermissionError
    hdu_b.writeto(fn_out)
    return 0


if __name__ == '__main__':
    mag_ab = 20.0 * u.ABmag
    wl = 1.1 * u.um
    dwl = 0.4 * u.um
    d_tel = 0.3 * u.m
    t_exp = 120.0 * u.s
    eff_inst = 0.9 * 0.7
    pix_fov = (2.0 * u.arcsec) ** 2
    fwhm = 1.5
    band = 'o'
    a = photons_total_at_band(mag_ab, band, d_tel, t_exp, eff_inst)

    fn = '/home/akitaya/research/hzg/photsim2/uvista{}.xy'.format(band)
    tbl = make_qtable(read_uvista_xyfile(fn), band, d_tel, t_exp, eff_inst, fwhm)
    print(id(tbl))
    tbl2 = qtable_dither(tbl, 0.1, 0.0)
    print(tbl2, tbl2.meta)
    print(id(tbl), id(tbl2))

    shape = (128, 128)  # Image dimension; (size_x, size_y).
    make_stellar_image_ndarray(tbl, shape, fn='testimg', show_elapsed_time=True)
    print(calc_zd_integ(0.5, 0.9, 0.0))
