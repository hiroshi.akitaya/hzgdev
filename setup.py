from setuptools import setup

setup(
    name='hzgdev',
    version='0.0.5',
    packages=['hzgdev'],
    url='https://gitlab.com/hiroshi.akitaya/hzgdev.git',
    license='TBD.',
    author='Hiroshi Akitaya',
    author_email='akitaya@perc.it-chiba.ac.jp',
    description='HiZ-GUNDAM Development Tools'
)
