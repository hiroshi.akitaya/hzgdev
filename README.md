# Hzgdev

Hi-Z GUNDAM development tools

## Getting started
1. Install this hgzdev package.
```
git clone https://gitlab.com/hiroshi.akitaya/hzgdev.git
cd hzgdev
pip install -e . # or pip install --user -e .
```

## Usage.

- see hzgdev/hzgdev/test/test01.py for creating artificial stellar image.
- Execute test script.
```
cd hzgdev/hzgdev/test
python test01.py
```

## Authors and acknowledgment

Hiroshi Akitaya (Chiba Institute of Technology)

## License

TBD.

## Project status

Under development.
